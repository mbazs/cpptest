#include <iostream>
#include "mystring.hpp"


int main(int argc, char *argv[])
{
    MyString::setDebug(true);

    MyString s1("hello");
    MyString s2 = s1;
    MyString s3;
    s3 = MyString("bello");

    std::cout << s3 << std::endl;
}
