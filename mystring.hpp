#ifndef MYSTRING_HPP
#define MYSTRING_HPP

#include <iostream>
#include <string>
#include <string.h>
#include <memory>

class MyString {
    std::string m_str;
    static bool m_debug;

public:
    MyString() {}
    MyString(char const *s):m_str(s) {
        if (m_debug)
            std::cerr << "char const *constructor" << std::endl;
    }
    static void setDebug(bool debug) { m_debug = debug; }

    MyString(MyString const &other) {
        if (m_debug)
            std::cerr << "copy constructor" << std::endl;
        m_str = other.m_str;
    }

    MyString& operator=(MyString const &other) {
        if (m_debug)
            std::cerr << "operator=" << std::endl;
        m_str = other.m_str;
    }

    MyString& operator=(MyString &&other) {
        if (m_debug)
            std::cerr << "rvalue operator=" << std::endl;
        m_str = std::move(other.m_str);
    }

    char const *c_str() {
        return m_str.c_str();
    }

    friend std::ostream &operator<<(std::ostream &os, MyString const &ms);

    ~MyString() {
    }
};

bool MyString::m_debug = false;

std::ostream &operator<<(std::ostream &os, MyString const &ms) {
    os << ms.m_str;
    return os;
}

#endif // MYSTRING_HPP
